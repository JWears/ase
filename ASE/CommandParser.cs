﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE
{
    public class CommandParser
    {
        Canvass MyCanvass;
        /// <summary>
        /// This public CommandParser holds the Canvass class and gives it the variable canvass, it is then used to allow methods from the Canvass to be used in this CommandParser class
        /// By Jack Wears.
        /// </summary>
        /// <param name="canvass">Holds the Canvass class and its methods</param>
        /// <returns>This returns the variable canvass which is used to give a MyCanvass varaible its value</returns>
        public CommandParser(Canvass canvass)
        {
            MyCanvass = canvass;
        }
        /// <summary>
        /// This method uses a string called Command which is given a string value for example a commandline textbox, this method will use a split array to seperate the values entered into the commandline
        /// the first value of this array is used for the intial command and the other two values of the array are used for its integer values. The MyCanvass variable is then used to call on the suitable method. seperate strings
        /// are also created for the second and third value of the command and are then converted to integers and used in the MyCanvass methods.
        /// By Jack Wears.
        /// </summary>
        /// <param name="Command">This holds the string text which is used for the specifc commands</param>
        /// <returns>This returns a string command used to draw commands</returns>
        /// <example>
        /// <code language="cs">
        /// // commandline
        /// <br></br>
        /// string Commands;
        /// <br></br>
        /// string[] commands = Commands.split(,);
        /// <br></br>
        /// string drawX, drawY; int valX, valY;
        /// <br></br>
        /// if(commands[0] == "drawto")
        /// <br></br>
        /// { 
        /// <br></br>
        /// drawX = Convert.ToInt32(valX); 
        /// <br></br>
        /// drawY = Convert.ToInt32(valY);
        /// <br></br>
        /// MyCanvass.PenDraw(valX,valY);
        /// <br></br>
        /// }
        /// </code>
        /// </example>
        public void CommandLine(string Command)  
        {
            
            String[] Commands = Command.Split(' ');
            String valueX, valueY;
            int drawValueX;
            int drawValueY;
            switch (Commands[0])
            {
                case "drawto":
                    {
                        if (Commands.Length > 3 || Commands.Length < 3)
                        {
                            MyCanvass.CommandLineError("Error with drawto parameters");
                        }
                        else
                        {
                            try
                            {
                                valueX = Commands[1];
                                valueY = Commands[2];
                                drawValueX = Convert.ToInt32(valueX);
                                drawValueY = Convert.ToInt32(valueY);
                                MyCanvass.PenDraw(drawValueX, drawValueY); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with drawto command");
                            }
                        }
                    }
                    break;

                case "moveto":
                    {
                        if (Commands.Length > 3 || Commands.Length < 3)
                        {
                            MyCanvass.CommandLineError("Error with moveto parameters");
                        }
                        else
                        {
                            try
                            {
                                valueX = Commands[1];
                                valueY = Commands[2];
                                drawValueX = Convert.ToInt32(valueX);
                                drawValueY = Convert.ToInt32(valueY);
                                MyCanvass.MoveTo(drawValueX, drawValueY); //draws set size line
                                MyCanvass.PenColourBlack();
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with moveto command");
                            }
                        }
                    }
                    break;

                case "rect":
                    {
                        if (Commands.Length > 3 || Commands.Length < 3)
                        {
                            MyCanvass.CommandLineError("Error with rect parameters");
                        }
                        else
                        {
                            try
                            {
                                valueX = Commands[1];
                                valueY = Commands[2];
                                drawValueX = Convert.ToInt32(valueX);
                                drawValueY = Convert.ToInt32(valueY);
                                MyCanvass.DrawRectangle(drawValueX, drawValueY); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with rect command");
                            }
                        }
                    }
                    break;

                case "circle":
                    {
                        if (Commands.Length > 2)
                        {
                            MyCanvass.CommandLineError("Error with circle parameters");
                        }
                        else
                        {
                            try
                            {
                                valueX = Commands[1];
                                drawValueX = Convert.ToInt32(valueX);
                                MyCanvass.DrawCircle(drawValueX); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with circle command");
                            }
                        }
                    }
                    break;

                case "triangle":
                    {

                        {
                            try
                            {

                                MyCanvass.DrawTriangle(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with triangle command");
                            }
                        }
                    }
                    break;

                case "penreset":
                    {

                        {
                            try
                            {

                                MyCanvass.PenReset(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with penreset command");
                            }
                        }
                    }
                    break;

                case "reset":
                    {

                        {
                            try
                            {

                                MyCanvass.PosReset(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with reset command");
                            }
                        }
                    }
                    break;

                case "clear":
                    {

                        {
                            try
                            {

                                MyCanvass.ClearCanvass(); //draws set size line
                                MyCanvass.PenReset();
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with reset command");
                            }
                        }
                    }
                    break;

                case "penred":
                    {

                        {
                            try
                            {

                                MyCanvass.PenColourRed(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with penred command");
                            }
                        }
                    }
                    break;

                case "penblue":
                    {

                        {
                            try
                            {

                                MyCanvass.PenColourBlue(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with penblue command");
                            }
                        }
                    }
                    break;

                case "pengreen":
                    {

                        {
                            try
                            {

                                MyCanvass.PenColourGreen(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with pengreen command");
                            }
                        }
                    }
                    break;

                case "penblack":
                    {

                        {
                            try
                            {

                                MyCanvass.PenColourBlack(); //draws set size line
                            }
                            catch
                            {
                                MyCanvass.CommandLineError("Error with penblack command");
                            }
                        }
                    }
                    break;

                default:
                    MyCanvass.CommandLineError("No valid command was provided");
                    break;
            }
        }
        /// <summary>
        /// This method uses a string called syntaxprogram which would be given a string value for example a program box of multiple lines, a string array will be used to seperate the values entered in the string array by a \n
        /// then a seperate array would be used to seperate this array into specific values. A for loop would be used to loop through the programbox text length and this would encase all the commands that would
        /// be used to draw onto the bitmap.
        /// By Jack Wears.
        /// </summary>
        /// <param name="syntaxprogram">This holds the string text which is used for the specifc commands</param>
        /// <returns>This returns a string command used to draw commands</returns>
        /// <example>
        /// <code language="cs">
        /// // programbox
        /// <br></br>
        /// string Commands;
        /// <br></br>
        /// string[] commands = Commands.split(,);
        /// <br></br>
        /// for loop
        /// <br></br>
        /// string[] comsyn = commands(\n);
        /// <br></br>
        /// string drawX, drawY; int valX, valY;
        /// <br></br>
        /// if(comsyn[0] == "drawto")
        /// <br></br>
        /// { 
        /// <br></br>
        /// drawX = Convert.ToInt32(valX); 
        /// <br></br>
        /// drawY = Convert.ToInt32(valY);
        /// <br></br>
        /// MyCanvass.PenDraw(valX,valY);
        /// <br></br>
        /// }
        /// </code>
        /// </example>
        public void SyntaxProgram(string syntaxprogram)
        {
            string valueX, valueY; // used to hold string value of parameters
            string valueVarString = ""; // holds var value as string
            string checkVar, checkVar2, stringCheck = ""; // used to check var values
            bool fill = false, varUse = false, ifUse = true; //both bools used to call on if set to true
            int drawValueX, drawValueY; // hold converted values of parameters
            int valueVarInt = 0; // int value of var
            int newValueVar; // new value of var used in operations
            int loopAmount; // holds loop amount
            int loopStart; // holds loop start
            int loopEnd = 0; // holds loop end
            string loopLine; //hold loop lines
            int ifVal, ifVal2; // hold both values in if statement
            string methodName = "NoMethod"; //holds method name
            string methodIs = "NoIsMethod"; // sets method is used as a check


            String[] syntaxLines = syntaxprogram.Split('\n'); // array of string by /n
            for (int i = 0; i < syntaxLines.Length; i++)
            {
                String[] CommandSyn = syntaxLines[i].Split(' ');

                switch (CommandSyn[0])
                {
                    case "drawto":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    try
                                    {
                                        if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                        {
                                            int varValueX = Convert.ToInt32(valueVarString);
                                            int varValueY = Convert.ToInt32(valueVarString);
                                            MyCanvass.PenDraw(varValueX, varValueY);
                                        }
                                        else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                            MyCanvass.PenDraw(drawValueX, drawValueY);
                                        }
                                        else
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        valueX = CommandSyn[1];
                                        valueY = CommandSyn[2];
                                        drawValueX = Convert.ToInt32(valueX);
                                        drawValueY = Convert.ToInt32(valueY);
                                        MyCanvass.PenDraw(drawValueX, drawValueY);
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }

                            }

                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }


                        }
                        break;

                    case "moveto":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }


                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    try
                                    {

                                        if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                        {
                                            int varValueX = Convert.ToInt32(valueVarString);
                                            int varValueY = Convert.ToInt32(valueVarString);
                                            MyCanvass.MoveTo(varValueX, varValueY);
                                            MyCanvass.PenColourBlack();
                                        }
                                        else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                            MyCanvass.MoveTo(drawValueX, drawValueY);
                                            MyCanvass.PenColourBlack();
                                        }
                                        else
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        valueX = CommandSyn[1];
                                        valueY = CommandSyn[2];
                                        drawValueX = Convert.ToInt32(valueX);
                                        drawValueY = Convert.ToInt32(valueY);
                                        MyCanvass.MoveTo(drawValueX, drawValueY);
                                        MyCanvass.PenColourBlack();
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }

                                
                            }

                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                        }
                        break;

                    case "rect":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                            {
                                                int varValueX = Convert.ToInt32(valueVarString);
                                                int varValueY = Convert.ToInt32(valueVarString);
                                                MyCanvass.FillRect(varValueX, varValueY);
                                            }
                                            else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                valueY = CommandSyn[2];
                                                drawValueX = Convert.ToInt32(valueX);
                                                drawValueY = Convert.ToInt32(valueY);
                                                MyCanvass.FillRect(drawValueX, drawValueY);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }

                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                            {
                                                int varValueX = Convert.ToInt32(valueVarString);
                                                int varValueY = Convert.ToInt32(valueVarString);
                                                MyCanvass.DrawRectangle(varValueX, varValueY);
                                            }
                                            else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                valueY = CommandSyn[2];
                                                drawValueX = Convert.ToInt32(valueX);
                                                drawValueY = Convert.ToInt32(valueY);
                                                MyCanvass.DrawRectangle(drawValueX, drawValueY);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                            MyCanvass.FillRect(drawValueX, drawValueY);
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                            MyCanvass.DrawRectangle(drawValueX, drawValueY);
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                }
                            }
                            //
                            else if (ifUse == false) 
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                        }
                        break;

                    case "rotate":
                        {
                            if (CommandSyn.Length > 2)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                            else if (ifUse == true) 
                            {

                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                MyCanvass.RotateShape(varValue);
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                MyCanvass.RotateShape(drawValueX);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                MyCanvass.RotateShape(varValue);
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                MyCanvass.RotateShape(drawValueX);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            MyCanvass.RotateShape(drawValueX); //draws set size line
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            MyCanvass.RotateShape(drawValueX); //draws set size line
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                }
                            }
                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                        }
                        break;

                    case "resetrotate":
                        {
                            MyCanvass.ResetRotate();
                        }
                        break;

                    case "circle":
                        {
                            if (CommandSyn.Length > 2)
                            {
                                MyCanvass.ProgramRuntimeError();

                            }

                            else if (ifUse == true)
                            {
                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                MyCanvass.FillCircle(varValue);
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                MyCanvass.FillCircle(drawValueX);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                MyCanvass.DrawCircle(varValue);
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                MyCanvass.DrawCircle(drawValueX);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramRuntimeError();
                                            }

                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            MyCanvass.FillCircle(drawValueX); //draws set size line
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            MyCanvass.DrawCircle(drawValueX); //draws set size line
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }



                                }
                            }
                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramRuntimeError();
                            }
                        }
                        break;

                    case "triangle":
                        {

                            {
                                if (fill == true)
                                {
                                    try
                                    {

                                        MyCanvass.FillTriangle(); //draws set size line
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }


                                else if (fill == false)
                                {
                                    try
                                    {

                                        MyCanvass.DrawTriangle(); //draws set size line
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }
                            }
                        }
                        break;

                    case "penreset":
                        {

                            {
                                try
                                {

                                    MyCanvass.PenReset();
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "reset":
                        {

                            {
                                try
                                {

                                    MyCanvass.PosReset(); //draws set size line
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "clear":
                        {

                            {
                                try
                                {

                                    MyCanvass.ClearCanvass(); //draws set size line
                                    MyCanvass.PenReset();
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "penred":
                        {

                            {
                                try
                                {

                                    MyCanvass.PenColourRed(); //draws set size line
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "penblue":
                        {

                            {
                                try
                                {

                                    MyCanvass.PenColourBlue(); //draws set size line
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "pengreen":
                        {

                            {
                                try
                                {

                                    MyCanvass.PenColourGreen(); //draws set size line
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "penblack":
                        {

                            {
                                try
                                {

                                    MyCanvass.PenColourBlack(); //draws set size line
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "redgreen":
                        {

                            {
                                try
                                {
                                    MyCanvass.PenColourFlashRedGreen();
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                        }
                        break;

                    case "fill":
                        {
                            fill = true;
                        }
                        break;

                    case "filloff":
                        {
                            fill = false;
                        }
                        break;

                    case "var":
                        {

                            if (CommandSyn.Length == 4) //var value = 5, 4 characters to set variable
                            {
                                try
                                {
                                    if (CommandSyn[2].Equals("="))
                                    {
                                        valueVarInt = Convert.ToInt32(CommandSyn[3]);
                                        valueVarString = CommandSyn[1];
                                        checkVar = CommandSyn[1];
                                        varUse = true;
                                        stringCheck = CommandSyn[1];
                                        valueVarString = Convert.ToString(valueVarInt);
                                    }
                                    else
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }

                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }

                            }
                            else if (CommandSyn.Length == 6) // var num = num +70, 6 characters to add expressions to variables
                            {
                                checkVar = CommandSyn[1]; // giving check var the value of the second value entered in command, which is the variable
                                checkVar2 = CommandSyn[3]; // same as line above but using a seperate value

                                if (stringCheck.Equals(checkVar) && stringCheck.Equals(checkVar2)) // checking if var used is same as var stated in string check
                                {

                                    if (CommandSyn[4].Equals("+")) // checing if operation used is + then adding current value of var to the new value of var
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]); // converting the new value to a int
                                            valueVarInt += newValueVar; // valueint = valueint + newvalue
                                            Console.WriteLine(newValueVar); // writing to console to check correct new value
                                            valueVarString = Convert.ToString(valueVarInt); // converting the new added value of var to string
                                        }
                                        catch 
                                        {

                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("-")) // same as + operation but using -
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt -= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch 
                                        { 
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("*"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt *= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch 
                                        {

                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("/"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt /= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch // add specifc format and divide by zero expections
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else
                                    {
                                        MyCanvass.ProgramRuntimeError(); // if correct operation isnt used then runtime error
                                    }
                                }
                                else 
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                            else 
                            {
                                MyCanvass.ProgramRuntimeError(); // if correct var command isnt used runtime error
                            }
                            Console.WriteLine(valueVarInt);
                            
                            
                        }
                        break;

                    case "loop":
                        {
                            if (CommandSyn.Length == 2) // ensuring the loop has a loop amount, loop 5 etc
                            {
                                // try
                                if (varUse == true)
                                {
                                    if (stringCheck.Equals(CommandSyn[1]))
                                    {
                                        try
                                        {
                                            int varValue = Convert.ToInt32(valueVarString);
                                            //loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                            for (int check = i; check < syntaxLines.Length; check++) // using for loop to loop through snytax length when check which = linenum "i" is less than length then adding 1 to check each loop
                                            {
                                                string lineCheck = syntaxLines[check]; // creating variable linecheck that equals each line in syntaxlines that is check in the for loop
                                                if (lineCheck.Equals("endloop")) // if the check = command endloop
                                                {
                                                    loopEnd = check; // gives endloop the position of "end" command                                        
                                                }
                                                
                                            }
                                            for (int x = 0; x <= varValue; x++) // creating a loop that loops the amount of times the loop amount is stated, loops all things by the loopamount then adds x++
                                            {
                                                loopStart = i+1; // giving start of loop value of linenum i, adding plus 1 as was reading loop line and causing error
                                                for (int s = loopStart; s < loopEnd; s++) // for loop inside of for loop, looping until loopstart < loopend, then s++
                                                {
                                                    //varUse = true;
                                                    loopLine = syntaxLines[s]; // string loopline holds the count that loops through syntaxlines
                                                    SyntaxProgram(loopLine); // calling syntaxprogram to run the loop lines
                                                }

                                            }
                                            i = loopEnd; // giving end of loop linenum value i
                                        }
                                        catch 
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                    else if (stringCheck != CommandSyn[1])
                                    {

                                        try
                                        {
                                            loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                            for (int check = i; check < syntaxLines.Length; check++)
                                            {
                                                string lineCheck = syntaxLines[check];
                                                if (lineCheck.Equals("endloop"))
                                                {
                                                    loopEnd = check;
                                                }
                                            }
                                            for (int x = 0; x <= loopAmount; x++)
                                            {
                                                loopStart = i;
                                                for (int s = loopStart; s < loopEnd; s++)
                                                {
                                                    //varUse = true;
                                                    loopLine = syntaxLines[s];
                                                    SyntaxProgram(loopLine);
                                                }

                                            }
                                            i = loopEnd; // giving end of loop linenum value i
                                        }
                                        catch 
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }

                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                        for (int check = i; check < syntaxLines.Length; check++)
                                        {
                                            string lineCheck = syntaxLines[check];
                                            if (lineCheck.Equals("endloop"))
                                            {
                                                loopEnd = check;
                                            }
                                        }
                                        for (int x = 0; x <= loopAmount; x++)
                                        {
                                            loopStart = i;
                                            for (int s = loopStart; s < loopEnd; s++)
                                            {
                                                //varUse = true;
                                                loopLine = syntaxLines[s];
                                                SyntaxProgram(loopLine);
                                            }

                                        }
                                        i = loopEnd; // giving end of loop linenum value i                                    
                                    }
                                    catch 
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }
                                }
                            }
                            else
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                        }
                        break;

                    case "if":
                        {
                            if (CommandSyn.Length == 4) // if 5 > 10
                            {
                                if (varUse == true) // if var is being used then
                                {
                                    if (stringCheck.Equals(CommandSyn[1])) // check if
                                    {
                                        try
                                        {
                                            int varValue = Convert.ToInt32(valueVarString); // getting
                                            ifVal2 = Convert.ToInt32(CommandSyn[3]);

                                            if (CommandSyn[2].Equals(">="))
                                            {
                                                if (varValue >= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("<="))
                                            {

                                                if (varValue <= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("=="))
                                            {
                                                if (varValue == ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("!="))
                                            {
                                                if (varValue != ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                    }
                                    else if (stringCheck != (CommandSyn[1]))
                                        try
                                        {
                                            ifVal = Convert.ToInt32(CommandSyn[1]);
                                            ifVal2 = Convert.ToInt32(CommandSyn[3]);
                                            if (CommandSyn[2].Equals(">="))
                                            {
                                                if (ifVal >= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("<="))
                                            {

                                                if (ifVal <= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("=="))
                                            {
                                                if (ifVal == ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("!="))
                                            {
                                                if (ifVal != ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramRuntimeError();
                                        }
                                }
                                else if (varUse == false) 
                                {
                                    try
                                    {
                                        ifVal = Convert.ToInt32(CommandSyn[1]);
                                        ifVal2 = Convert.ToInt32(CommandSyn[3]);
                                        if (CommandSyn[2].Equals(">="))
                                        {
                                            if (ifVal >= ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("<="))
                                        {

                                            if (ifVal <= ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("=="))
                                        {
                                            if (ifVal == ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("!="))
                                        {
                                            if (ifVal != ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramRuntimeError();
                                    }


                                }

                            }
                            else
                            {
                                MyCanvass.ProgramRuntimeError();
                            }
                        }
                        break;

                    case "method":
                        {
                            if (CommandSyn.Length == 2) // ensuring the loop has a loop amount, loop 5 etc
                            {
                                try
                                {
                                    methodName = CommandSyn[1];
                                    for (int check = i; check < syntaxLines.Length; check++) // using for loop to loop through snytax length when check which = linenum "i" is less than length then adding 1 to check each loop
                                    {
                                        string lineCheck = syntaxLines[check]; // creating variable linecheck that equals each line in syntaxlines that is check in the for loop
                                        if (lineCheck.Equals("endmethod")) // if the check = command endloop
                                        {
                                            loopEnd = check; // gives endloop the position of "end" command                                        
                                        }
                                    }

                                    for (int x = 1; x <= 1; x++)
                                    {
                                        loopStart = i;
                                        for (int s = loopStart; s < loopEnd; s++)
                                        {
                                            if (methodName == methodIs)
                                            {
                                                loopLine = syntaxLines[s];
                                                SyntaxProgram(loopLine);
                                            }
                                            else 
                                            {

                                                MyCanvass.ProgramRuntimeError();
                                            }
                                        }

                                    }
                                    i = loopEnd; // giving end of loop linenum value i    
                                }
                                catch 
                                {

                                    MyCanvass.ProgramRuntimeError();
                                
                                }
                            }
                        }
                        break;
                    case "method_is":
                        {

                            methodIs = CommandSyn[1];
                        }
                        break;

                    case "string":
 {
                            if (CommandSyn.Length >= 2)
                            {
                                try
                                {
                                    string[] split = CommandSyn[1].Split('"');
                                    MyCanvass.StringCommand(split[1], i * 15);
                                }
                                catch
                                {
                                    MyCanvass.ProgramRuntimeError();
                                }
                            }
                            else
                            {
                                MyCanvass.ProgramRuntimeError();
                            }

                        }
                        break;

                    default:
                        MyCanvass.ProgramRuntimeError();
                        break;

                }
            }
        }
        /// <summary>
        /// This method uses a string called syntaxcheckprogram which would be given a string value for example a program box of multiple lines, a string array will be used to seperate the values entered in the string array by a \n,
        /// after this this array will be split by another string array into specific values. This is simliar to the SyntaxProgram method but doesnt draw anything onto the bitmap when the correct commands are
        /// called on.
        /// By Jack Wears.
        /// </summary>
        /// <param name="syntaxcheckprogram">This holds the string text which is used for the specifc check commands</param>
        /// <returns>This returns a string command used to check the draw commands and if they are invalid print a specific error string</returns>
        /// <example>
        /// <code language="cs">
        /// // check programbox
        /// <br></br>
        /// string Commands;
        /// <br></br>
        /// string[] commands = Commands.split(,);
        /// <br></br>
        /// for loop
        /// <br></br>
        /// string[] comsyn = commands(\n);
        /// <br></br>
        /// string drawX, drawY; int valX, valY;
        /// <br></br>
        /// if(comsyn[0] == "drawto")
        /// <br></br>
        /// { 
        /// <br></br>
        /// try{
        /// <br></br>
        /// drawX = Convert.ToInt32(valX); 
        /// <br></br>
        /// drawY = Convert.ToInt32(valY);
        /// <br></br>
        /// } catch{ MyCanvass.ProgramError(string "bad code", int 1, int 0) }
        /// <br></br>
        /// }
        /// </code>
        /// </example>
        public void SyntaxCheck(string syntaxcheckprogram)
        {
            string valueX, valueY;
            string valueVarString = "";
            string checkVar, checkVar2, stringCheck = "";
            bool fill = false, varUse = false, ifUse = true;
            int drawValueX, drawValueY;
            int valueVarInt = 0;
            int newValueVar;
            int loopAmount;
            int loopStart;
            int loopEnd = 0;
            string loopLine;
            int ifVal, ifVal2;
            string methodName = "NoMethod";
            string methodIs = "NoIsMethod";

            String[] syntaxLines = syntaxcheckprogram.Split('\n');
            for (int i = 0; i < syntaxLines.Length; i++)
            {
                String[] CommandSyn = syntaxLines[i].Split(' ');
                int yPos = i * 15; // changes as count goes up 15, 30, 45 etc
                switch (CommandSyn[0])
                {
                    case "drawto":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramError("Error with drawto parameters amount. Line: ", i + 1, yPos);
                            }

                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    try
                                    {
                                        if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                        {
                                            int varValueX = Convert.ToInt32(valueVarString);
                                            int varValueY = Convert.ToInt32(valueVarString);
                                        }
                                        else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                        }
                                        else
                                        {
                                            MyCanvass.ProgramError("Error with drawto parameter type. Line: ", i + 1, yPos); //linenum + 1 to stop linenum starting at 0
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with drawto parameter type. Line: ", i + 1, yPos);
                                    }
                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        valueX = CommandSyn[1];
                                        valueY = CommandSyn[2];
                                        drawValueX = Convert.ToInt32(valueX);
                                        drawValueY = Convert.ToInt32(valueY);
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with drawto parameter type. Line: ", i + 1, yPos);
                                    }
                                }

                            }

                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramError("if statement is not valid, try a valid statement. Line: ", i + 1, yPos);
                            }
                        }
                        break;

                    case "moveto":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramError("Error with moveto parameters amount. Line: ", i + 1, yPos);
                            }


                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    try
                                    {

                                        if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                        {
                                            int varValueX = Convert.ToInt32(valueVarString);
                                            int varValueY = Convert.ToInt32(valueVarString);
                                        }
                                        else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                        }
                                        else
                                        {
                                            MyCanvass.ProgramError("Error with moveto parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with moveto parameter type. Line: ", i + 1, yPos);
                                    }
                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        valueX = CommandSyn[1];
                                        valueY = CommandSyn[2];
                                        drawValueX = Convert.ToInt32(valueX);
                                        drawValueY = Convert.ToInt32(valueY);
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with moveto parameter type. Line: ", i + 1, yPos);
                                    }
                                }


                            }

                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramError("if statement is not valid, try a valid statement. Line: ", i + 1, yPos);
                            }
                        }
                        break;

                    case "rect":
                        {
                            if (CommandSyn.Length > 3 || CommandSyn.Length < 3)
                            {
                                MyCanvass.ProgramError("Error with rect parameters amount. Line: ", i + 1, yPos);
                            }

                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                            {
                                                int varValueX = Convert.ToInt32(valueVarString);
                                                int varValueY = Convert.ToInt32(valueVarString);
                                            }
                                            else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                valueY = CommandSyn[2];
                                                drawValueX = Convert.ToInt32(valueX);
                                                drawValueY = Convert.ToInt32(valueY);
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                            }

                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]) && stringCheck.Equals(CommandSyn[2]))
                                            {
                                                int varValueX = Convert.ToInt32(valueVarString);
                                                int varValueY = Convert.ToInt32(valueVarString);
                                               
                                            }
                                            else if (CommandSyn[1] != stringCheck && CommandSyn[2] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                valueY = CommandSyn[2];
                                                drawValueX = Convert.ToInt32(valueX);
                                                drawValueY = Convert.ToInt32(valueY);
                                                
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                           
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            valueY = CommandSyn[2];
                                            drawValueX = Convert.ToInt32(valueX);
                                            drawValueY = Convert.ToInt32(valueY);
                                            
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rect parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                }
                            }
                            //
                            else if (ifUse == false) 
                            {
                                MyCanvass.ProgramError("if statement is not valid, try a valid statement. Line: ", i + 1, yPos);
                            }
                        }
                        break;

                    case "rotate":
                        {
                            if (CommandSyn.Length > 2)
                            {
                                MyCanvass.ProgramError("Error with rotate parameter amount. Line: ", i + 1, yPos);
                            }

                            else if (ifUse == true)
                            {

                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                               
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with rotate parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                }
                            }
                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramError("if statement is not valid, try a valid statement. Line: ", i + 1, yPos);
                            }
                        }
                        break;

                    case "circle":
                        {
                            if (CommandSyn.Length > 2)
                            {
                                MyCanvass.ProgramError("Error with circle parameter amount. Line:  ", i + 1, yPos);

                            }

                            else if (ifUse == true)
                            {
                                if (varUse == true)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                         
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                               
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            if (stringCheck.Equals(CommandSyn[1]))
                                            {
                                                int varValue = Convert.ToInt32(valueVarString);
                                                
                                            }
                                            else if (CommandSyn[1] != stringCheck)
                                            {
                                                valueX = CommandSyn[1];
                                                drawValueX = Convert.ToInt32(valueX);
                                                
                                            }
                                            else
                                            {
                                                MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                            }

                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                        }
                                    }
                                }

                                else if (varUse == false)
                                {
                                    if (fill == true)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (fill == false)
                                    {
                                        try
                                        {
                                            valueX = CommandSyn[1];
                                            drawValueX = Convert.ToInt32(valueX);
                                            
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Error with circle parameter type. Line: ", i + 1, yPos);
                                        }
                                    }



                                }
                            }
                            else if (ifUse == false)
                            {
                                MyCanvass.ProgramError("if statement is not valid, try a valid statement. Line: ", i + 1, yPos);
                            }

                        }
                        break;

                    case "triangle":
                        {

                            {
                                if (fill == true)
                                {
                                    try
                                    {

                                       
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with triangle. Line:", i + 1, yPos);
                                    }
                                }


                                else if (fill == false)
                                {
                                    try
                                    {

                                        
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Error with triangle. Line: ", i + 1, yPos);
                                    }
                                }
                            }
                        }
                        break;

                    case "penreset":
                        {

                            {
                                try
                                {

                                    
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with penreset. Line: ", i + 1, yPos);
                                }
                            }
                        }
                        break;

                    case "reset":
                        {

                            {
                                try
                                {

                                   
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with rest. Line:", i + 1, yPos);
                                }
                            }
                        }
                        break;

                    case "clear":
                        {

                            {
                                try
                                {

                                   
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with clear. Line:", i + 1, yPos);
                                }
                            }
                        }
                        break;

                    case "penred":
                        {

                            {
                                try
                                {

                                    
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with penred. Line: ", i+1, yPos);
                                }
                            }
                        }
                        break;

                    case "penblue":
                        {

                            {
                                try
                                {

                                    
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with penblue. Line:", i+1, yPos);
                                }
                            }
                        }
                        break;

                    case "pengreen":
                        {

                            {
                                try
                                {

                                    
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with pengreen. Line:", i+1, yPos);
                                }
                            }
                        }
                        break;

                    case "penblack":
                        {

                            {
                                try
                                {

                                    
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Error with penblack. Line:", i+1, yPos);
                                }
                            }
                        }
                        break;

                    case "fill":
                        {
                            fill = true;
                        }
                        break;

                    case "filloff":
                        {
                            fill = false;
                        }
                        break;

                    case "var":
                        {

                            if (CommandSyn.Length == 4) //var value = 5, 4 characters to set variable
                            {
                                try
                                {
                                    if (CommandSyn[2].Equals("="))
                                    {
                                        valueVarInt = Convert.ToInt32(CommandSyn[3]);
                                        valueVarString = CommandSyn[1];
                                        checkVar = CommandSyn[1];
                                        varUse = true;
                                        stringCheck = CommandSyn[1];
                                        valueVarString = Convert.ToString(valueVarInt);
                                    }
                                    else
                                    {
                                        MyCanvass.ProgramError("Invaild operation used to state variable. Line: ", i+1, yPos);
                                    }

                                }
                                catch
                                {
                                    MyCanvass.ProgramError("Invalid value attributed to var command. Line: ",i+1,yPos);
                                }

                            }
                            else if (CommandSyn.Length == 6)
                            {
                                checkVar = CommandSyn[1]; 
                                checkVar2 = CommandSyn[3]; 

                                if (stringCheck.Equals(checkVar) && stringCheck.Equals(checkVar2))
                                {

                                    if (CommandSyn[4].Equals("+"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt += newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch
                                        {

                                            MyCanvass.ProgramError("Invalid value attributed to var + operation. Line: ", i+1, yPos);
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("-"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt -= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Invalid value attributed to var - operation. Line: ", i+1, yPos);
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("*"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt *= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch
                                        {

                                            MyCanvass.ProgramError("Invalid value attributed to var * operation. Line: ", i+1, yPos);
                                        }
                                    }

                                    else if (CommandSyn[4].Equals("/"))
                                    {
                                        try
                                        {
                                            newValueVar = Convert.ToInt32(CommandSyn[5]);
                                            valueVarInt /= newValueVar;
                                            Console.WriteLine(newValueVar);
                                            valueVarString = Convert.ToString(valueVarInt);
                                        }
                                        catch (FormatException)
                                        {
                                            MyCanvass.ProgramError("Invalid value attributed to var / operation. Line: ", i + 1, yPos);
                                        }
                                        catch (DivideByZeroException) 
                                        {
                                            MyCanvass.ProgramError("Cannot divide value by zero, try different value. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else
                                    {
                                        MyCanvass.ProgramError("No valid operation has been used. Line: ", i+1, yPos); // if correct operation isnt used then runtime error
                                    }
                                }
                                else
                                {
                                    MyCanvass.ProgramError("Valid variable is not used. Line: ", i+1, yPos);
                                }
                            }
                            else
                            {
                                MyCanvass.ProgramError("Invalid variable command used. Line: ", i+1, yPos); // if correct var command isnt used runtime error
                            }
                            Console.WriteLine(valueVarInt);


                        }
                        break;

                    case "loop":
                        {
                            if (CommandSyn.Length == 2) // ensuring the loop has a loop amount, loop 5 etc
                            {
                                // try
                                if (varUse == true)
                                {
                                    if (stringCheck.Equals(CommandSyn[1]))
                                    {
                                        try
                                        {
                                            int varValue = Convert.ToInt32(valueVarString);
                                            //loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                            for (int check = i; check < syntaxLines.Length; check++) // using for loop to loop through snytax length when check which = linenum "i" is less than length then adding 1 to check each loop
                                            {
                                                string lineCheck = syntaxLines[check]; // creating variable linecheck that equals each line in syntaxlines that is check in the for loop
                                                if (lineCheck.Equals("endloop")) // if the check = command endloop
                                                {
                                                    loopEnd = check; // gives endloop the position of "end" command                                        
                                                }
                                            }
                                            for (int x = 0; x <= varValue; x++) // creating a loop that loops the amount of times the loop amount is stated, loops all things by the loopamount then adds x++
                                            {
                                                loopStart = i + 1; // giving start of loop value of linenum i, adding plus 1 as was reading loop line and causing error
                                                for (int s = loopStart; s < loopEnd; s++) // for loop inside of for loop, looping until loopstart < loopend, then y++
                                                {
                                                    //varUse = true;
                                                    loopLine = syntaxLines[s]; // string loopline holds the count that loops through syntaxlines
                                                    // // calling syntaxprogram to run the loop lines
                                                }

                                            }
                                            i = loopEnd; // giving end of loop linenum value i
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Invaild value used for loop. Line: ", i + 1, yPos);
                                        }
                                    }

                                    else if (stringCheck != CommandSyn[1])
                                    {

                                        try
                                        {
                                            loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                            for (int check = i; check < syntaxLines.Length; check++)
                                            {
                                                string lineCheck = syntaxLines[check];
                                                if (lineCheck.Equals("endloop"))
                                                {
                                                    loopEnd = check;
                                                }
                                            }
                                            for (int x = 0; x <= loopAmount; x++)
                                            {
                                                loopStart = i;
                                                for (int s = loopStart; s < loopEnd; s++)
                                                {
                                                    //varUse = true;
                                                    loopLine = syntaxLines[s];
                                                    //
                                                }

                                            }
                                            i = loopEnd; // giving end of loop linenum value i
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Invaild value used for loop. Line: ", i + 1, yPos);
                                        }
                                    }

                                }
                                else if (varUse == false)
                                {
                                    try
                                    {
                                        loopAmount = Convert.ToInt32(CommandSyn[1]); // convert loop amount to int value


                                        for (int check = i; check < syntaxLines.Length; check++)
                                        {
                                            string lineCheck = syntaxLines[check];
                                            if (lineCheck.Equals("endloop"))
                                            {
                                                loopEnd = check;
                                            }
                                        }
                                        for (int x = 0; x <= loopAmount; x++)
                                        {
                                            loopStart = i;
                                            for (int s = loopStart; s < loopEnd; s++)
                                            {
                                                //varUse = true;
                                                loopLine = syntaxLines[s];
                                                //
                                            }

                                        }
                                        i = loopEnd; // giving end of loop linenum value i                                    
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Invaild value used for loop. Line: ", i + 1, yPos);
                                    }
                                }
                            }
                            else
                            {
                                MyCanvass.ProgramError("Invaild syntax for command loop: ", i + 1, yPos);
                            }

                        }
                        break;

                    case "if":
                        {
                            if (CommandSyn.Length == 4) // if 5 > 10
                            {
                                if (varUse == true) // if var is being used then
                                {
                                    if (stringCheck.Equals(CommandSyn[1])) // check if
                                    {
                                        try
                                        {
                                            int varValue = Convert.ToInt32(valueVarString); // getting
                                            ifVal2 = Convert.ToInt32(CommandSyn[3]);

                                            if (CommandSyn[2].Equals(">="))
                                            {
                                                if (varValue >= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("<="))
                                            {

                                                if (varValue <= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("=="))
                                            {
                                                if (varValue == ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("!="))
                                            {
                                                if (varValue != ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Invaild value used for if statement. Line: ", i + 1, yPos);
                                        }
                                    }
                                    else if (stringCheck != (CommandSyn[1]))
                                        try
                                        {
                                            ifVal = Convert.ToInt32(CommandSyn[1]);
                                            ifVal2 = Convert.ToInt32(CommandSyn[3]);
                                            if (CommandSyn[2].Equals(">="))
                                            {
                                                if (ifVal >= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("<="))
                                            {

                                                if (ifVal <= ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("=="))
                                            {
                                                if (ifVal == ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                            else if (CommandSyn[2].Equals("!="))
                                            {
                                                if (ifVal != ifVal2)
                                                {
                                                    ifUse = true;
                                                }
                                                else
                                                {
                                                    ifUse = false;
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            MyCanvass.ProgramError("Invaild value used for if statement. Line: ", i + 1, yPos);
                                        }
                                }


                                else if (varUse == false)
                                {
                                    try
                                    {
                                        ifVal = Convert.ToInt32(CommandSyn[1]);
                                        ifVal2 = Convert.ToInt32(CommandSyn[3]);
                                        if (CommandSyn[2].Equals(">="))
                                        {
                                            if (ifVal >= ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("<="))
                                        {

                                            if (ifVal <= ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("=="))
                                        {
                                            if (ifVal == ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                        else if (CommandSyn[2].Equals("!="))
                                        {
                                            if (ifVal != ifVal2)
                                            {
                                                ifUse = true;
                                            }
                                            else
                                            {
                                                ifUse = false;
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        MyCanvass.ProgramError("Invaild value used for if statement. Line: ", i + 1, yPos);
                                    }


                                }

                            }
                            else
                            {
                                MyCanvass.ProgramError("Invaild type of if command used. Line: ", i + 1, yPos);
                            }
                        }
                        break;

                    case "method":
                        {
                            if (CommandSyn.Length == 2) // ensuring the loop has a loop amount, loop 5 etc
                            {
                                try
                                {
                                    methodName = CommandSyn[1];
                                    for (int check = i; check < syntaxLines.Length; check++) // using for loop to loop through snytax length when check which = linenum "i" is less than length then adding 1 to check each loop
                                    {
                                        string lineCheck = syntaxLines[check]; // creating variable linecheck that equals each line in syntaxlines that is check in the for loop
                                        if (lineCheck.Equals("endmethod")) // if the check = command endloop
                                        {
                                            loopEnd = check; // gives endloop the position of "end" command                                        
                                        }
                                    }

                                    for (int x = 1; x <= 1; x++)
                                    {
                                        loopStart = i;
                                        for (int s = loopStart; s < loopEnd; s++)
                                        {
                                            if (methodName == methodIs)
                                            {
                                                loopLine = syntaxLines[s];
                                                //
                                            }
                                            else
                                            {

                                                MyCanvass.ProgramError("Method name is invalid or not been stated. Line: ", i + 1, yPos);
                                            }
                                        }

                                    }
                                    i = loopEnd; // giving end of loop linenum value i    
                                }
                                catch
                                {

                                    MyCanvass.ProgramError("Invaild value used for method. Line: ", i + 1, yPos);

                                }
                            }
                        }
                        break;
                    case "method_is":
                        {

                            methodIs = CommandSyn[1];
                        }
                        break;

                    case "string":
                        {
                            if (CommandSyn.Length >= 2)
                            {
                                try
                                {
                                    string[] split = CommandSyn[1].Split('"');
                                    MyCanvass.StringCommand(split[1], i * 15);
                                }
                                catch
                                {
                                    MyCanvass.ProgramError("invalid string command. Line: ", i + 1, yPos);
                                }
                            }
                            else 
                            {
                                MyCanvass.ProgramError("invalid string command. Line: ",i+1,yPos);
                            }
                        }
                        break;

                    default:
                        MyCanvass.ProgramError("Invaild command given. Line: ", i+1, yPos);
                        break;
                }

            }
        }
    }
}
 