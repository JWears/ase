﻿
namespace ASE
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExitButton = new System.Windows.Forms.Button();
            this.syntaxTextBox = new System.Windows.Forms.RichTextBox();
            this.Canvas = new System.Windows.Forms.PictureBox();
            this.commandLine = new System.Windows.Forms.RichTextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.Syntaxtbutton = new System.Windows.Forms.Button();
            this.RunButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ColourButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
            this.SuspendLayout();
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(297, 395);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 23);
            this.ExitButton.TabIndex = 1;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // syntaxTextBox
            // 
            this.syntaxTextBox.Location = new System.Drawing.Point(55, 33);
            this.syntaxTextBox.Name = "syntaxTextBox";
            this.syntaxTextBox.Size = new System.Drawing.Size(317, 324);
            this.syntaxTextBox.TabIndex = 2;
            this.syntaxTextBox.Text = "";
            // 
            // Canvas
            // 
            this.Canvas.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Canvas.Location = new System.Drawing.Point(399, 33);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(323, 356);
            this.Canvas.TabIndex = 3;
            this.Canvas.TabStop = false;
            this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(55, 364);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(317, 25);
            this.commandLine.TabIndex = 4;
            this.commandLine.Text = "";
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(566, 395);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 5;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(647, 395);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 6;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // Syntaxtbutton
            // 
            this.Syntaxtbutton.Location = new System.Drawing.Point(218, 395);
            this.Syntaxtbutton.Name = "Syntaxtbutton";
            this.Syntaxtbutton.Size = new System.Drawing.Size(75, 23);
            this.Syntaxtbutton.TabIndex = 7;
            this.Syntaxtbutton.Text = "Syntax";
            this.Syntaxtbutton.UseVisualStyleBackColor = true;
            this.Syntaxtbutton.Click += new System.EventHandler(this.Syntaxtbtn_Click);
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(55, 396);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(75, 23);
            this.RunButton.TabIndex = 8;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(137, 396);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 9;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ColourButton
            // 
            this.ColourButton.Location = new System.Drawing.Point(728, 33);
            this.ColourButton.Name = "ColourButton";
            this.ColourButton.Size = new System.Drawing.Size(60, 42);
            this.ColourButton.TabIndex = 10;
            this.ColourButton.Text = "Colour change";
            this.ColourButton.UseVisualStyleBackColor = true;
            this.ColourButton.Click += new System.EventHandler(this.ColourButton_Click);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ColourButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.Syntaxtbutton);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.Canvas);
            this.Controls.Add(this.syntaxTextBox);
            this.Controls.Add(this.ExitButton);
            this.Name = "GUI";
            this.Text = "GUI";
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.RichTextBox syntaxTextBox;
        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.RichTextBox commandLine;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button Syntaxtbutton;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ColourButton;
    }
}

