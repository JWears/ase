﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE
{
    public partial class GUI : Form
    {
        private const int X = 640;
        private const int Y = 640;
        private static readonly int ScreenX = X;
        private static readonly int ScreenY = Y;

        //creating bitmap will be used to display graphics on picturebox
        readonly Bitmap OutputBitmap = new Bitmap(ScreenX, ScreenY);
        readonly Canvass MyCanvass;
        CommandParser parser;
        /// <summary>
        /// Public GUI used to initialize component, create a variable for the Canvass class and CommandParser class
        /// By Jack Wears.
        /// </summary>
        public GUI()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(OutputBitmap)); //having variable draw on specified bitmap
            parser = new CommandParser(MyCanvass); // creating variable that holds class commandParser methods
            
        }
        /// <summary>
        /// This method checks the textbox commandline and if the event of a key being pressed down, then having this keycode equal the specific key enter to siginify the command being run. After this is, it checks the text entered in
        /// the command line and if it equals run it shall run the SyntaxProgram() method in Command Parsers class and if not it shall run the CommandLine() method.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender"> sender contains a reference to the object that raised the event</param>
        /// <param name="e"> e contains the event data for KeyEventArgs</param>
        /// <returns>This method returns the KeyEvent enter to the object</returns>
        /// <example>
        /// <code language="cs">
        ///  // commandline keydown
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "run")
        /// <br></br> 
        /// { parser.SyntaxProgram(syntaxBox); }
        /// <br></br>
        /// else if (command != "run")
        ///  <br></br>
        ///  { parser.CommandLine(CommandCL); }
        /// <br></br>
        /// </code>
        /// </example>
        public void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String CommandCL = commandLine.Text.Trim().ToLower(); //string for commandline as command breaks becuase its assigned to commands[0]
                String Command = commandLine.Text.Trim().ToLower();//reading commandLine box then trimming blank spaces and converting all to lower case
                String[] Commands = Command.Split(' '); // to delete
                Command = Commands[0]; // to delete
                String syntaxBox = syntaxTextBox.Text.Trim().ToLower();

                if (Command == "run") // if commandline gets run command
                {
                    parser.SyntaxProgram(syntaxBox); // run program box
                }
                if (Command !=  "run") // if does not equal run
                {
                    parser.CommandLine(CommandCL); // run commandline       
                }
                
                commandLine.Text = "";//clearing commandLine box
                Refresh();//refreshing the display
            }
        }
        /// <summary>
        /// This method uses a paint event and sender to draw on a defined bitmap at the point 0,0.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for PaintEventArgs</param>
        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }
        /// <summary>
        /// This method exits the GUI window and uses a EventArgs to call when the exit button is clicked.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void exitButton_Click(object sender, EventArgs e)
        {
            Close(); //Closes form window
        }
        /// <summary>
        /// This method uses the SaveFileDialog class to allow the user to access a file save dialog then save the program they have created in the programbox.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void saveButton_Click(object sender, EventArgs e) //added recently
        {
            SaveFileDialog sFd = new SaveFileDialog();

            if (sFd.ShowDialog() == DialogResult.OK) // if the user click ok button
            {
                syntaxTextBox.SaveFile(sFd.FileName, RichTextBoxStreamType.PlainText);
            }
        }
        /// <summary>
        /// This method uses a OpenFileDialog class to allow the user to a open a open file dialog and select a file to display in the program box.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void loadButton_Click(object sender, EventArgs e) //added recently
        {
            OpenFileDialog oFd = new OpenFileDialog();

            if (oFd.ShowDialog() == DialogResult.OK) // if the user click ok button
            {
                syntaxTextBox.Text = File.ReadAllText(oFd.FileName);
            }
        }
        /// <summary>
        /// This method uses the EventArgs so when the syntax button is click it runs the CommnadParser method syntax check and checks the text written in the program box and prints its errors, then refreshing the bitmap.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void Syntaxtbtn_Click(object sender, EventArgs e)
        {
            string syntaxcheck;
            syntaxcheck = syntaxTextBox.Text.Trim().ToLower();
            parser.SyntaxCheck(syntaxcheck);
            Refresh();
        }
        /// <summary>
        /// This method uses the EventArgs so when the run button is clicked it uses the CommandParser method SyntaxProgram() and uses the text written in the program box for this method, then refreshing the bitmap.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void RunButton_Click(object sender, EventArgs e)
        {
            string syn = syntaxTextBox.Text.Trim().ToLower();
            parser.SyntaxProgram(syn);
            Refresh();
        }
        /// <summary>
        /// This method uses EventArgs so when the clear button is clicked it uses the Canvass methods ClearCanvass(), PenReset(), PenColourBlack() to clear the drawings that have been made on the bitmap
        /// reset it colour back to default and the pen aswell, then refreshing the bitmap.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            MyCanvass.ClearCanvass();
            MyCanvass.PenReset();
            MyCanvass.PenColourBlack();
            Refresh();
        }
        /// <summary>
        /// This method uses EventArgs so when the colour button is clicked it uses the Canvass method ColourChange() to change the colour of the pen.
        /// By Jack Wears.
        /// </summary>
        /// <param name="sender">sender contains a reference to the object that raised the event</param>
        /// <param name="e">e contains the event data for EventArgs</param>
        private void ColourButton_Click(object sender, EventArgs e)
        {
            MyCanvass.ColourChange();
        }

    }
}
