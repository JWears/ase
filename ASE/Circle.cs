﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE
{
    class Circle : Shape
    {
        int radius;

        public Circle(Color colour, int x, int y, int radius, bool fill) : base(colour, x, y, fill)
        {
            this.radius = radius;     
        }

        public override void draw(Graphics g)
        {
            Pen pen = new Pen(Color.Black, 3); //use colour variable
            SolidBrush brush = new SolidBrush(Color.Black); //use colour variable
            g.DrawEllipse(pen, x, y, radius * 2, radius * 2);
            g.FillEllipse(brush, x, y, radius * 2, radius * 2);
            
        }


    }
}
