﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE
{
    public class Command
    {
        public Command()
        {

        }
        /// <summary>
        /// This method tests the parsing of the drawto command and the two int values that would be used in the drawto command, it does this by spliting the command into a array then if the first
        /// value of this array equals drawto the method will proceed to convert the second and third part the array into a int value.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the int param, which is both the second and third int value entered in the command but then converted to integers</returns>
        public int DrawTo(string command)
        {
            int drawValueX, drawValueY;
            int param = 0;
            String[] Commands = command.Split(' ');
            string valueX;
            string valueY;

            if (Commands[0].Equals("drawto"))
            {
                valueX = Commands[1];
                valueY = Commands[2];
                drawValueX = Convert.ToInt32(valueX);
                drawValueY = Convert.ToInt32(valueY);
                param = drawValueX + drawValueY;
            }
            return param;
        }
        /// <summary>
        /// This method tests the parsing of the drawto command checking that a invalid value of the two int values that would be caught if they are strings, it does this by spliting the command into a array then if the first
        /// value of this array equals drawto the method will proceed to convert the second and third part the array into a int value and using a try catch to catch the ormat expection.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the bool parsetest which by default is set to true, however when a failed parse happens it will be switch to false</returns>
        public bool DrawToTest(string command)
        {
            int drawValueX, drawValueY;
            int param = 0;
            String[] Commands = command.Split(' ');
            string valueX;
            string valueY;
            bool parsetest = true;

            if (Commands[0].Equals("drawto"))
            {
                try
                {
                    parsetest = true;
                    valueX = Commands[1];
                    valueY = Commands[2];
                    drawValueX = Convert.ToInt32(valueX);
                    drawValueY = Convert.ToInt32(valueY);
                    param = drawValueX + drawValueY;
                }
                catch 
                {
                    parsetest = false;
                }
            }
            return parsetest;
        }
        /// <summary>
        /// This method tests the parsing of the moveto command and the two int values that would be used in the moveto command, it does this by spliting the command into a array then if the first
        /// value of this array equals moveto the method will proceed to convert the second and third part the array into a int value.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the int param, which is both the second and third int value entered in the command but then converted to integers</returns>
        public int MoveTo(string command)
        {
            int drawValueX, drawValueY;
            int param = 0;
            String[] Commands = command.Split(' ');
            string valueX;
            string valueY;

            if (Commands[0].Equals("moveto"))
            {
                valueX = Commands[1];
                valueY = Commands[2];
                drawValueX = Convert.ToInt32(valueX);
                drawValueY = Convert.ToInt32(valueY);
                param = drawValueX + drawValueY;
            }
            return param;
        }
        /// <summary>
        /// This method tests the parsing of the drawrect command and the two int values that would be used in the drawrect command, it does this by spliting the command into a array then if the first
        /// value of this array equals rect the method will proceed to convert the second and third part the array into a int value.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the int param, which is both the second and third int value entered in the command but then converted to integers</returns>
        public int Drawrect(string command)
        {
            int drawValueX, drawValueY;
            int param = 0;
            String[] Commands = command.Split(' ');
            string valueX;
            string valueY;

            if (Commands[0].Equals("rect"))
            {
                valueX = Commands[1];
                valueY = Commands[2];
                drawValueX = Convert.ToInt32(valueX);
                drawValueY = Convert.ToInt32(valueY);
                param = drawValueX + drawValueY;
            }
            return param;
        }
        /// <summary>
        /// This method tests the parsing of the drawcircle command and the single int value that would be used in the drawcircle command, it does this by spliting the command into a array then if the first
        /// value of this array equals circle the method will proceed to convert the second part the array into a int value.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the int param, which is the second int value entered in the command but then converted to integers</returns>
        public int DrawCircle(string command)
        {
            int drawValueX;
            int param = 0;
            String[] Commands = command.Split(' ');
            string valueX;

            if (Commands[0].Equals("circle"))
            {
                valueX = Commands[1];
                drawValueX = Convert.ToInt32(valueX);
                param = drawValueX;
            }
            return param;
        }
        /// <summary>
        /// This method tests the setting of a var value, and it does this by spliting the command into a array then checking this array is equal to four which is the string amount needed to set a var eg //var num = 90. After this
        /// it checks if the third command eqauls =, then converting the forth part of the array into a int.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the valueVarInt and the new value it will have been set</returns>
        public int VarSet(string command)
        {
            String[] Commands = command.Split(' ');
            int valueVarInt = 0;
            string valueVarString;
            string stringCheck;
            string checkVar;


            if (Commands.Length == 4) //var value = 5, 4 characters to set variable
            {
                if (Commands[2].Equals("="))
                {
                    valueVarInt = Convert.ToInt32(Commands[3]);
                    valueVarString = Commands[1];
                    checkVar = Commands[1];
                    stringCheck = Commands[1];
                    valueVarString = Convert.ToString(valueVarInt);

                }


            }
            return valueVarInt;
        }
        /// <summary>
        /// This method tests the var command and if it is being used, it does this by having a boolean called varUse which is set to false by default, but when the correct parameters are met it switches the boolean to true
        /// and allows the var to be used.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the boolean varuse and will return true or false depending on if the command used is valid</returns>
        public bool VarSetCheckUse(string command)
        {
            String[] Commands = command.Split(' ');
            int valueVarInt = 0;
            string valueVarString;
            bool varUse = false;
            string stringCheck;
            string checkVar;


            if (Commands.Length == 4) //var value = 5, 4 characters to set variable
            {
                if (Commands[2].Equals("="))
                {
                    valueVarInt = Convert.ToInt32(Commands[3]);
                    valueVarString = Commands[1];
                    checkVar = Commands[1];
                    varUse = true;
                    stringCheck = Commands[1];
                    valueVarString = Convert.ToString(valueVarInt);
                }


            }
            return varUse;
        }
        /// <summary>
        /// This method tests the setting of a new var value to the current variable, and it does this by spliting the command into a array then checking this array is equal to six which is the string amount needed to set a new var eg //var num = num + 90. After this
        /// it checks if the third command eqauls =, then converting the forth part of the array into a int.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the valueVarInt and the new value it will have been set</returns>
        public int VarSetNewValue(string command)
        {
            String[] Commands = command.Split(' ');
            int valueVarInt = 0;
            string valueVarString;
            bool varUse;
            string stringCheck;
            string checkVar,checkVar2;
            int newValueVar;

            if (Commands.Length == 6) // var num = num +70, 6 characters to add expressions to variables
            {
                stringCheck = Commands[1];
                checkVar = Commands[1];
                checkVar2 = Commands[3];

                if (stringCheck.Equals(checkVar) && stringCheck.Equals(checkVar2))
                {

                    if (Commands[4].Equals("+"))
                    {

                        newValueVar = Convert.ToInt32(Commands[5]);
                        valueVarInt += newValueVar;
                        valueVarString = Convert.ToString(valueVarInt); // converting the new added value of var to string

                    }

                    
                }

            }
            return valueVarInt;
        }
        /// <summary>
        /// This method tests a IfStatement with a boolean, and when this statement is correct it will return a true boolean showing the statement has been processed, eg. // if 5>= 3
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns a boolean ifUse that returns a true value when a correct if statement is given but returns a false by default or when a incorrect statement is given</returns>
        public bool IfStatement(string command) 
        {
            String[] Commands = command.Split(' ');
            int ifVal, ifVal2;
            bool ifUse = false;


            if (Commands.Length == 4)
            {
                ifVal = Convert.ToInt32(Commands[1]);
                ifVal2 = Convert.ToInt32(Commands[3]);
                if (Commands[2].Equals(">="))
                {
                    if (ifVal >= ifVal2)
                    {
                        ifUse = true;
                    }
                    else
                    {
                        ifUse = false;
                    }
                }
            }
            return ifUse;
        }
        /// <summary>
        /// This method tests a IfStatement with a boolean, and when this statement is incorrect it will return a false boolean showing the statement has been processed, eg. // if 5 >= 9
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns a boolean ifUse that returns a true value when a correct if statement is given or by default but returns a false when a incorrect statement is given</returns>
        public bool IfStatementTest(string command)
        {
            String[] Commands = command.Split(' ');
            int ifVal, ifVal2;
            bool ifUse = true;


            if (Commands.Length == 4)
            {
                ifVal = Convert.ToInt32(Commands[1]);
                ifVal2 = Convert.ToInt32(Commands[3]);
                if (Commands[2].Equals(">="))
                {
                    if (ifVal >= ifVal2)
                    {
                        ifUse = true;
                    }
                    else
                    {
                        ifUse = false;
                    }
                }
            }
            return ifUse;
        }
        /// <summary>
        /// This method tests the loop command and the parsing of the amount of loops that are given, it does this by spliting the command string into a array then checking if the length is eqaul to 2
        /// then converting the second value of the array into a int value.
        /// By Jack Wears.
        /// </summary>
        /// <param name="command">This holds the string which will contain the command that will be parsed</param>
        /// <returns>This method returns the loopAmount which is converted the second value of a array converted into a int value</returns>
        public int LoopNumTest(string command) 
        {
            String[] Commands = command.Split(' ');
            int loopAmount = 0;
            if (Commands.Length == 2) // ensuring the loop has a loop amount, loop 5 etc
            {
                loopAmount = Convert.ToInt32(Commands[1]);
            }
            return loopAmount;
        }

    }
}
