﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE
{
    public class Canvass
    {

        // Entry point to the class initiating graphics, pen and solidbrush giving them all variables, also initiating the X and Y positions as int variables.

        Graphics g; //giving GDI a variable
        Pen pen; //creating pen
        SolidBrush solidBrush;  //creating solidbrush
        int xPos, yPos; //creating x and y postitions for drawing
        Boolean flag = false, Keepruuning = true ;

        /// <summary>
        /// Setting the instance of the Graphics g variable, giving the X and Y position a value of 0 , and setting the instance of the pen variable and giving it a default colour.
        /// By Jack Wears.
        /// </summary>
        /// <param name="g">Graphics variable used for drawing</param>
        /// <example>
        /// <code language="cs">
        ///  // Draw line
        /// <br></br>
        /// g.Drawline(pen,100,100,50,50);
        /// <br></br>
        /// </code>
        /// </example>
        public Canvass(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            pen = new Pen(Color.Black, 3); // giving pen default colour black
            solidBrush = new SolidBrush(Color.Black);
        }

        /// <summary>
        /// This method will use the graphics pen to draw a line from the current X(xPos) and Y(yPos) position and draw it to the stated X(toX) and Y(toY) position.
        /// By Jack Wears.
        /// </summary>
        /// <param name="toX"> The X cord that will be entered as a parameter</param>
        /// <param name="toY">The Y cord that will be entered as a parameter</param>
        /// <returns>This will return a new x and y axis that the pen will be drawn to in the current pen colour</returns>
        /// <example>
        /// <code language="cs">
        ///  // Draw line
        /// <br></br>
        /// int valueX = 50; int valueY = 100;
        /// <br></br>
        /// DrawTo(valueX, valueY);
        /// </code>
        /// </example>
        public void PenDraw(int toX, int toY)
        {
            g.DrawLine(pen, xPos, yPos, toX, toY); //drawing line to set postion x and y
            xPos = toX;
            yPos = toY;
        }
        /// <summary>
        /// This method will move the pen from current X and Y position to the stated X and Y position in a transparent line.
        /// By Jack Wears.
        /// </summary>
        /// <param name="toX">The X cord that will be entered as a parameter</param>
        /// <param name="toY">The Y cord that will be entered as a parameter</param>
        /// <returns>This will return a new x and y axis that the pen will be drawn to in a transparent colour</returns>
        /// <example>
        /// <code language="cs">
        ///  // moveto position
        /// <br></br>
        /// int valueX = 50; int valueY = 100;
        /// <br></br>
        /// MoveTo(valueX, valueY);
        /// </code>
        /// </example>
        public void MoveTo(int toX, int toY)
        {
            pen = new Pen(Color.Transparent, 1);
            g.DrawLine(pen, xPos, yPos, toX, toY); //drawing line to specified postion x and y
            xPos = toX;
            yPos = toY;
            
        }
        /// <summary>
        /// This method will draw a rectangle using the current X and Y position and the specified width and length of the rectangle.
        /// By Jack Wears.
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="length">Length of the rectangle</param>
        /// <returns>This will return two integer values to draw a rectangle according to its x and y position</returns>
        /// <example>
        /// <code language="cs">
        ///  // Draw rectangle
        /// <br></br>
        /// int valueX = 50; int valueY = 50;
        /// <br></br>
        /// DrawRectangle(valueX, valueY);
        /// </code>
        /// </example>

        public void DrawRectangle(int width, int length)
        {
            g.DrawRectangle(pen, xPos, yPos, xPos + width, yPos + length); //using draw rectangle with x and y pos and the created int width and length to draw rectangle
            
        }
        /// <summary>
        /// This method will draw a circle using the current X and Y position and the specified radius of the circle.
        /// By Jack Wears.
        /// </summary>
        /// <param name="radius">Radius of circle</param>
        /// <returns>This will return a single integer values to draw a circle according to its x and y position and radius calucation</returns>
        /// <example>
        /// <code language="cs">
        ///  // Draw circle
        /// <br></br>
        /// int valueX = 50;
        /// <br></br>
        /// DrawCircle(valueX);
        /// </code>
        /// </example>
        public void DrawCircle(int radius)//created int radius to specifiy size of circle
        {

            g.DrawEllipse(pen, xPos, yPos, xPos + (radius * 2), yPos + (radius * 2)); //using draw ellipse to create circle multiplying radius by 2 for both x and y pos
        }
        /// <summary>
        /// This method will draw a triangle using a point array and specifiying all points of the triangle in the array.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // Draw triangle
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "triangle")
        ///  <br></br>
        ///  { DrawTriangle(); }
        /// </code>
        /// </example>
        public void DrawTriangle()
        {
           Point[] point = new Point[3]; // creating points array

            point[0].X = 150;
            point[0].Y = 100;

            point[1].X = 150;
            point[1].Y = 200;

            point[2].X = 90;
            point[2].Y = 100; // specifing points of the triangle
            g.DrawPolygon(pen,point); //using pen to draw points
           

        }
        /// <summary>
        /// This method will reset the pen position back to 0 for both positions X and Y then reset the colour to default black, it will also reset the transformation.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // Draw reset pen
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "resetpen")
        ///  <br></br>
        ///  { PenReset();  }
        /// </code>
        /// </example>
        public void PenReset() 
        {
            xPos = yPos = 0; //sets pen position to 0
            pen = new Pen(Color.Black, 3); //sets pen color back to default
            g.ResetTransform();
        }
        /// <summary>
        /// This method will reset the pen position back to 0 for both positions X and Y, and it will reset the transformation.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // Draw reset pen pos
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "resetpos")
        ///  <br></br>
        ///  { PosReset();  }
        /// </code>
        /// </example>
        public void PosReset() 
        {
            xPos = yPos = 0; //sets pen position to 0
            g.ResetTransform();
        }
        /// <summary>
        /// This method will clear the canvass by changing all colours drawn to transparent.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // clear canvass
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "clear")
        ///  <br></br>
        ///  { ClearCanvass();  }
        /// </code>
        /// </example>
        public void ClearCanvass() 
        {
            g.Clear(Color.Transparent);
        }
        /// <summary>
        /// This method will change both the pen and solidbrush to colour red.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen to red
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "penred")
        ///  <br></br>
        ///  { PenColourRed();  }
        /// </code>
        /// </example>
        public void PenColourRed() 
        {
            pen = new Pen(Color.Red, 3);
            solidBrush = new SolidBrush(Color.Red); // added so when pen col changed changes the fill col aswell
        }
        /// <summary>
        /// This method will change both the pen and solidbrush to colour blue.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen to blue
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "penblue")
        ///  <br></br>
        ///  { PenColourBlue();  }
        /// </code>
        /// </example>
        public void PenColourBlue() 
        {
            pen = new Pen(Color.Blue, 3);
            solidBrush = new SolidBrush(Color.Blue);
        }
        /// <summary>
        /// This method will change both the pen and solidbrush to colour green.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen to green
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "pengreen")
        ///  <br></br>
        ///  { PenColourGreen();  }
        /// </code>
        /// </example>
        public void PenColourGreen() 
        {
            pen = new Pen(Color.GreenYellow, 3);
            solidBrush = new SolidBrush(Color.GreenYellow);
        }
        /// <summary>
        /// This method will change both the pen and solidbrush to colour black.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen to black
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "penblack")
        ///  <br></br>
        ///  { PenColourBlack();  }
        /// </code>
        /// </example>
        public void PenColourBlack()
        {
            pen = new Pen(Color.Black, 3);
            solidBrush = new SolidBrush(Color.Black);
        }
        /// <summary>
        /// This method starts a thread that uses a different method to have this thread constantly change colours of the pen.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen to flash red and green
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "redgreen")
        ///  <br></br>
        ///  { PenColourFlashRedGreen();  }
        /// </code>
        /// </example>
        public void PenColourFlashRedGreen()
        {

                Thread thread = new Thread(ThreadMethodRG);
                thread.Start();
        }
        /// <summary>
        /// Method draws a preset rectangle in a constant while loop, having the colour of the pen change everytime the flag value equals true or false.
        /// By Jack Wears.
        /// </summary>
        public void ThreadMethodRG()
        {


            
            while (Keepruuning)
            {
                Rectangle rect = new Rectangle(100, 100, 100, 100);
                if (flag == false)
                {
                    lock (pen)
                    {
                        pen.Color = Color.Red;
                        g.DrawRectangle(pen, rect);
                        flag = true;
                    }
                    
                    
                }
                else if(flag == true)
                {
                    lock (pen)
                    {
                        pen.Color = Color.LightGreen;
                        g.DrawRectangle(pen, rect);
                        flag = false;
                    }
                    

                }
                Thread.Sleep(1000);
            }
        }
        /// <summary>
        /// This method will draw a fill rectangle using the current X and Y position and the specified width and length of the rectangle.
        /// By Jack Wears.
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="length">Length of the rectangle</param>
        /// <returns>This will return two integer values to draw a filled rectangle according to its x and y position</returns>
        /// <example>
        /// <code language="cs">
        ///  // Draw fill rectangle
        /// <br></br>
        /// int valueX = 50; int valueY = 50;
        /// <br></br>
        /// FillRectangle(valueX, valueY);
        /// </code>
        /// </example>
        public void FillRect(int width, int length)
        {
            
            g.FillRectangle(solidBrush, xPos, yPos, xPos + width, yPos + length);

        }
        /// <summary>
        /// This method will draw a fill circle using the current X and Y position and the specified radius of the circle.
        /// By Jack Wears.
        /// </summary>
        /// <param name="radius">Radius of circle</param>
        /// <returns>This will return a single integer value to draw a filled circle according to its x and y position</returns>
        /// <example>
        /// <code language="cs">
        ///  // Draw fill circle
        /// <br></br>
        /// int valueX = 50;
        /// <br></br>
        /// FillCircle(valueX);
        /// </code>
        /// </example>
        public void FillCircle(int radius)
        {
            
            g.FillEllipse(solidBrush, xPos, yPos, xPos + (radius * 2), yPos + (radius * 2));

        }
        /// <summary>
        /// This method will draw a fill triangle using a point array and specifiying all points of the triangle in the array.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // Draw fill triangle
        /// <br></br>
        /// string command;
        /// <br></br>
        ///  if (command == "filltriangle")
        ///  <br></br>
        ///  { FillTriangle(); }
        /// </code>
        /// </example>
        public void FillTriangle()
        {

            Point[] point = new Point[3]; // creating points array

            point[0].X = 150;
            point[0].Y = 100;

            point[1].X = 150;
            point[1].Y = 200;

            point[2].X = 90;
            point[2].Y = 100; // specifing points of the triangle
            g.FillPolygon(solidBrush, point);
        }
        /// <summary>
        /// This method will draw a string onto the bitmap using graphics drawstring and the font class for the programbox, using the error as a string that will be typed with "", a line int that will display the line num with the error string, then a yPos which will determine where the string will be printed on the y axis, x axis is default 0.
        /// By Jack Wears.
        /// </summary>
        /// <param name="Error"> Will hold the error message in a string</param>
        /// <param name="line">Will hold the line number in a int and print with error</param>
        /// <param name="yPos">Will yPos of string determining where it will print</param>
        /// <returns>This method returns the error string which is typed in the method and the line number that this error is found, then it sets the yPos of this error string</returns>
        /// <example>
        /// <code language="cs">
        /// // Write program error
        /// <br></br>
        /// int linenum = 1; int yPos = linenum*15;
        /// <br></br>
        /// string error = "Invalid command here at line: ";
        /// <br></br>
        /// ProgramError(error,linenum,yPos);
        /// </code>
        /// </example>

        public void ProgramError(string Error, int line, int yPos)
        {
            Font font = new Font("Calibri", 10);
            g.DrawString(Error + line, font, solidBrush, 0,yPos);
        }
        /// <summary>
        /// This method will draw a string onto the bitmap graphics drawstring and the font class for the commandline, will print the string error that is typed in at x which is 0.
        /// By Jack Wears.
        /// </summary>
        /// <param name="Error">Will hold the error message in a string</param>
        /// <returns>This method returns the error string which is typed in the method</returns>
        /// <example>
        /// <code language="cs">
        /// // Write commandline error
        /// <br></br>
        /// string error = "Invalid command here in CommandLine: ";
        /// <br></br>
        /// CommandLineError(error);
        /// </code>
        /// </example>
        public void CommandLineError(string Error)
        {
            int x = 0;
            Font font = new Font("Calibri", 10);
            g.DrawString(Error, font, solidBrush, x, x);
        }
        /// <summary>
        /// This method will draw a default string onto the bitmap using graphics drawstring and the font class for the programbox.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// // Write runtime program error
        /// <br></br>
        /// <br></br>
        /// string command;
        /// <br></br>
        /// if (command != "drawto")
        /// <br></br>
        /// { ProgramRuntimeError(); }
        /// </code>
        /// </example>
        public void ProgramRuntimeError()
        {
            int x = 0;
            Font font = new Font("Calibri", 10);
            g.DrawString("Error during runtime please use syntax button to see error", font, solidBrush, x, x);
        }
        /// <summary>
        /// This method will change colour of the pen and solid brush depending on which colour is selected when in the colour dialog
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        ///  // change pen colour
        /// <br></br>
        /// if (command == "changecolour")
        /// <br></br>
        /// { ColourChange(); }
        /// </code>
        /// </example>
        public void ColourChange() 
        {
            ColorDialog colorDialog = new ColorDialog();
            
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                pen.Color = colorDialog.Color;
                solidBrush.Color = colorDialog.Color;
            }
        }
        /// <summary>
        /// This method changes the angle that commands are drawn in having a angle int that allows for specific angles.
        /// By Jack Wears.
        /// </summary>
        /// <param name="angle">Will hold the angle that changes how the shape is drawn</param>
        /// <returns>This will return a single integer value to change the angle any command that is used after to draw it</returns>
        /// <example>
        /// <code language="cs">
        ///  // rotate shapes
        /// <br></br>
        /// int valueX = 50;
        /// <br></br>
        /// RotateShape(valueX);
        /// </code>
        /// </example>
        public void RotateShape(int angle)
        {
            
            g.RotateTransform(angle);
        }
        /// <summary>
        /// This method resets the angle of drawing to default, that the rotate shape command changes.
        /// By Jack Wears.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// //reset rotation
        /// <br></br>
        /// if (command == "resetrotate")
        /// <br></br>
        /// { ResetRotate(); }
        /// </code>
        /// </example>
        public void ResetRotate()
        {
            g.ResetTransform();
        }
        /// <summary>
        /// This method takes a string from the user then prints it onto the bitmap.
        /// By Jack Wears.
        /// </summary>
        /// <param name="comstring">Command string that holds the entered string in the method</param>
        /// <param name="yPos">Ypos holds the ypos of the string</param>
        /// <example>
        /// <code language="cs">
        /// // Write string
        /// <br></br>
        /// string comstring = "hello_world";
        /// <br></br>
        /// int yPos = 15;
        /// <br></br>
        /// StringCommand(comstring,yPos);
        /// </code>
        /// </example>
        public void StringCommand(string comstring, int yPos)
        {
            Font font = new Font("Calibri", 10);
            g.DrawString(comstring, font, solidBrush, 0, yPos);
        }

    }
}