﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE
{
    abstract public class Shape
    {
        public int x;
        public int y;
        protected Color colour;
        public bool fill;

        public Shape(Color colour, int x, int y, bool fill)
        {
            this.colour = colour; //shapes colour
            this.x = x; //its xpos
            this.y = y; //its ypos
            this.fill = fill; // shapes fill on or off filter
        }

        public abstract void draw(Graphics g);




    }
}
