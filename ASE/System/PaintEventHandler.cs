﻿using System.Windows.Forms;

namespace System
{
    internal class PaintEventHandler
    {
        private Action<object, PaintEventArgs> pictureBox1_Click;

        public PaintEventHandler(Action<object, PaintEventArgs> pictureBox1_Click)
        {
            this.pictureBox1_Click = pictureBox1_Click;
        }
    }
}