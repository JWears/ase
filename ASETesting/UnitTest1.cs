using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE;
using System.Drawing;
using System;

namespace ASETesting
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Here testing the drawto method checking if the numbers are being parsed then adding them together and checking if its the same as the expected result
        /// </summary>
        [TestMethod]
        public void DrawTo_Paramters_Parse()
        {
            Command command = new Command();
            int s;
            string line = "drawto 100 100";
            s = command.DrawTo(line);
            int test = 200;

            Assert.AreEqual(test, s);
        }
        /// <summary>
        /// Testing the invalid parse seeing if a string command would be caught by the try catch and produce a false result
        /// </summary>
        [TestMethod]
        public void DrawToTest_Paramters_Parse_Invalid()
        {
            Command command = new Command();
            bool s;
            string line = "drawto gfh 100";
            s = command.DrawToTest(line);
            bool test = false;

            Assert.AreEqual(test, s);
        }
        /// <summary>
        /// Here testing the moveot method checking if the numbers are being parsed then adding them together and checking if its the same as the expected result
        /// </summary>
        [TestMethod]
        public void MoveTo_Paramters_Parse()
        {
            Command command = new Command();
            int s;
            string line = "moveto 100 100";
            s = command.MoveTo(line);
            int test = 200;

            Assert.AreEqual(test, s);
        }

        /// <summary>
        /// Here testing the drawrect method checking if the numbers are being parsed then adding them together and checking if its the same as the expected result
        /// </summary>
        [TestMethod]
        public void Rect_Paramters_Parse()
        {
            Command command = new Command();
            int s;
            string line = "rect 100 100";
            s = command.Drawrect(line);
            int test = 200;
            
            Assert.AreEqual(test, s);
        }

        /// <summary>
        /// Here testing the circle method checking if the value is being parsed and converted to a integer then checking if its the same as the expected result
        /// </summary>
        [TestMethod]
        public void Circle_Paramters_Parse()
        {
            Command command = new Command();
            int s;
            string line = "circle 100";
            s = command.DrawCircle(line);
            int test = 100;

            Assert.AreEqual(test, s);
        }
        /// <summary>
        /// Testing setvar method checking the variable number has been set and parsed
        /// </summary>
        [TestMethod]
        public void SetVar_Value_Parse() 
        {
            Command command = new Command();
            int s;
            string line = "var num = 100";
            s = command.VarSet(line);
            int test = 100;

            Assert.AreEqual(test, s);

        }
        /// <summary>
        /// Testing if varuse if true when valid command is given or false when a invalid command is given, this enables varuse use bool
        /// </summary>
        [TestMethod]
        public void SetVar_VarUse_ReturnsTrue()
        {
            Command command = new Command();
            bool s, s2;
            string line = "var num = 100";
            string line2 = "var num 89 66";
            s = command.VarSetCheckUse(line);
            s2 = command.VarSetCheckUse(line2);
            bool test = true;
            bool test2 = false;

            Assert.AreEqual(test, s);
            Assert.AreEqual(test2, s2);
        }
        /// <summary>
        /// Testing setvar new value method, which checks the new variable has been parsed successfully
        /// </summary>
        [TestMethod]
        public void SetNewVar_Value_Parse()
        {
            Command command = new Command();
            int s;
            string line = "var num = num + 60";
            s = command.VarSetNewValue(line);
            int test = 60;

            Assert.AreEqual(test, s);

        }

        /// <summary>
        /// Testing if statement will return true when a valid command is given to method
        /// </summary>

        [TestMethod]
        public void IfStatement_IfUse_ReturnsTrue()
        {
            {
                Command command = new Command();
                bool s;
                string line = "if 5 >= 4";
                s = command.IfStatement(line);
                bool test = true;

                Assert.AreEqual(test, s);
            }
        }
        /// <summary>
        /// Testing if statement will return false when a invalid command is given to method
        /// </summary>
        [TestMethod]
        public void IfStatement_IfUse_ReturnsFalse()
        {
            {
                Command command = new Command();
                bool s;
                string line = "if 5 >= 8";
                s = command.IfStatementTest(line);
                bool test = false;

                Assert.AreEqual(test, s);
            }
        }
        /// <summary>
        /// Testing if loop amount will be parse to a int successfully
        /// </summary>
        [TestMethod]
        public void LoopNum_Value_Parse()
        {
            {
                Command command = new Command();
                int s;
                string line = "loop 5";
                s = command.LoopNumTest(line);
                int test = 5;

                Assert.AreEqual(test, s);

            }
        }

    }
}
